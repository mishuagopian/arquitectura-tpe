/*********************************************
kasm.h

************************************************/

#include "defs.h"

#define TTICKS_PER_SECOND 18

unsigned int    _read_msw();

void            _lidt (IDTR *idtr);

void		_mascaraPIC1 (byte mascara);  /* Escribe mascara de PIC1 */
void		_mascaraPIC2 (byte mascara);  /* Escribe mascara de PIC2 */

void		_Cli(void);        /* Deshabilita interrupciones  */
void		_Sti(void);	 /* Habilita interrupciones  */

void		_int_08_hand();      /* Timer tick */
void		_int_09_hand();      /* Teclado */
void		_int_74_hand();      /* Mouse */
void		_int_80_hand();	     /* Syscalls */
void		_system_call();

void		_debug (void);

