#include "../include/kasm.h"
#include "../include/defs.h"
#include "../include/visual.h"
#include "../include/shell.h"
#include "../include/keyboard.h"
#include "../include/mouse.h"
#include "../include/commands.h"
#include "../include/rtc.h"

DESCR_INT idt[0xA0];			/* IDT de 160 entradas*/
IDTR idtr;				/* IDTR */





/***************************************************************
*setup_IDT_entry
* Inicializa un descriptor de la IDT
*
*Recibe: Puntero a elemento de la IDT
*	 Selector a cargar en el descriptor de interrupcion
*	 Puntero a rutina de atencion de interrupcion	
*	 Derechos de acceso del segmento
*	 Cero
****************************************************************/

void setup_IDT_entry (DESCR_INT *item, byte selector, dword offset, byte access,
			 byte cero) {
  item->selector = selector;
  item->offset_l = offset & 0xFFFF;
  item->offset_h = offset >> 16;
  item->access = access;
  item->cero = cero;
}





/************************************************
kmain() 
Punto de entrada de código C.
*************************************************/

void load_IDT_entries(){
/* CARGA DE IDT CON LA RUTINA DE ATENCION DE IRQ0 - timerTick */
        setup_IDT_entry (&idt[0x08], 0x08, (dword)&_int_08_hand, ACS_INT, 0);
/* CARGA DE IDT CON LA RUTINA DE ATENCION DE IRQ1 - teclado */
        setup_IDT_entry (&idt[0x09], 0x08, (dword)&_int_09_hand, ACS_INT, 0);
/* CARGA DE IDT CON LA RUTINA DE ATENCION DE IRQ12 - Mouse */
        setup_IDT_entry (&idt[0x74], 0x08, (dword)&_int_74_hand, ACS_INT, 0);
/* CARGA DE IDT CON LA RUTINA DE ATENCION DE LA INT 80h */
        setup_IDT_entry (&idt[0x80], 0x08, (dword)&_int_80_hand, ACS_INT, 0);
}

void load_IDTR(){
/* Carga de IDTR    */
	idtr.base = 0;  
	idtr.base +=(dword) &idt;
	idtr.limit = sizeof(idt)-1;
	_lidt (&idtr);	
}


kmain() 
{
	load_IDT_entries();
	load_IDTR();

	_Cli();
        _mascaraPIC1(0xF8);
        _mascaraPIC2(0xEF);
	_Sti();
	
	initialize_screen();
	initialize_shell();
	initialize_keyboard();
	initialize_mouse();
	initialize_rtc();

	show_shell();	
}

