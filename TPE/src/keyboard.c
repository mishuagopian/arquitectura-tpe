#include "../include/keyboard.h"

static unsigned int keyTable[][2] = { { NOT_PRINTABLE, NOT_PRINTABLE },/*000*/
    { NOT_PRINTABLE, NOT_PRINTABLE },/*001 ESCAPE*/
    { '1', '!' }, /*002*/
    { '2', '\"' }, /*003*/
    { '3', '#' }, /*004*/
    { '4', '$' }, /*005*/
    { '5', '%' }, /*006*/
    { '6', '&' }, /*007*/
    { '7', '/' }, /*008*/
    { '8', '(' }, /*009*/
    { '9', ')' }, /*010*/
    { '0', '=' }, /*011*/
    { '\'', '?' }, /*012*/
    { '\n', '\n' }, /*013*/
    { '\b', '\b' }, /*014 BACKSPACE*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*015 TAB*/
    { 'q', 'Q' }, /*016*/
    { 'w', 'W' }, /*017*/
    { 'e', 'E' }, /*018*/
    { 'r', 'R' }, /*019*/
    { 't', 'T' }, /*020*/
    { 'y', 'Y' }, /*021*/
    { 'u', 'U' }, /*022*/
    { 'i', 'I' }, /*023*/
    { 'o', 'O' }, /*024*/
    { 'p', 'P' }, /*025*/
    { '\'', '\"' }, /*026*/
    { '+', '*' }, /*027*/
    { '\n', '\n' }, /*028*/
    { NOT_PRINTABLE, NOT_PRINTABLE },/*029 CTRL IZQ*/
    { 'a', 'A' }, /*030*/
    { 's', 'S' }, /*031*/
    { 'd', 'D' }, /*032*/
    { 'f', 'F' }, /*033*/
    { 'g', 'G' }, /*034*/
    { 'h', 'H' }, /*035*/
    { 'j', 'J' }, /*036*/
    { 'k', 'K' }, /*037*/
    { 'l', 'L' }, /*038*/
    { '.', ';' }, /*039*/
    { '{', '[' }, /*040*/
    { '}', ']' }, /*041*/
    { NOT_PRINTABLE, NOT_PRINTABLE },/*042 SHIFT IZQ*/
    { '<', '>' }, /*043*/
    { 'z', 'Z' }, /*044*/
    { 'x', 'X' }, /*045*/
    { 'c', 'C' }, /*046*/
    { 'v', 'V' }, /*047*/
    { 'b', 'B' }, /*048*/
    { 'n', 'N' }, /*049*/
    { 'm', 'M' }, /*050*/
    { ',', ';' }, /*051*/
    { '.', ':' }, /*052*/
    { '?', '?' }, /*053*/
    { NOT_PRINTABLE, NOT_PRINTABLE },/*054 SHIFT DER*/
    { '*', '*' }, /*055 KEY **/
    { NOT_PRINTABLE, NOT_PRINTABLE },/*056 ALT IZQ*/
    { ' ', ' ' }, /*057 SPACE*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*058 CAPSLOCK*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*059 F1*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*060 F2*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*061 F3*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*062 F4*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*063 F5*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*064 F6*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*065 F7*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*066 F8*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*067 F9*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*068 F10*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*069 NUM LOCK*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*070 SCROLL LOCK*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*071 KEY 7*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*072 KEY 8*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*073 KEY 9*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*074 KEY -*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*075 KEY 4*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*076 KEY 5*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*077 KEY 6*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*078 KEY +*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*079 KEY 1*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*080 KEY 2*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*081 KEY 3*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*082 KEY 0*/
    { NOT_PRINTABLE, NOT_PRINTABLE }, /*083 KEY .*/
    { NOT_PRINTABLE, NOT_PRINTABLE },/*084 SYS REQ (AT)*/
    { '+', '*' }, /*085*/
    { '+', '*' }, /*086*/
    { NOT_PRINTABLE, NOT_PRINTABLE },/*087 F11*/
    { NOT_PRINTABLE, NOT_PRINTABLE },/*088 F12*/
    { '+', '*' }, /*089*/
    { '+', '*' } /*090*/
};

Keyboard keyboard;

void initialize_keyboard(){
	keyboard.canRead=0;
	keyboard.bufferIndexR=0;
	keyboard.bufferIndexW=0;
	keyboard.caps=0;
	keyboard.shifted=0;
}

int is_letter(int scancode){
	return (scancode>=16 && scancode<=25) || (scancode>=30 && scancode<=38) || (scancode>=44 && scancode<=50);
}

int is_makecode(int scancode){
	return scancode<128;
}

void add_to_buffer(char c){
	keyboard.buffer[keyboard.bufferIndexW] = c;
	if(keyboard.bufferIndexW < KEYBOARD_BUFFER_SIZE-1){	
		keyboard.bufferIndexW++;
	}
	else{
		keyboard.bufferIndexW=0;
	}
	keyboard.canRead++;
}


unsigned int can_read(){
	return keyboard.canRead;
}

unsigned char read_from_buffer(){
	if(!keyboard.canRead){
		return 0;
	}
	char aux=keyboard.buffer[keyboard.bufferIndexR];
	if(keyboard.bufferIndexR < KEYBOARD_BUFFER_SIZE-1){
		keyboard.bufferIndexR++;
	}
	else{
		keyboard.bufferIndexR=0;
	}
	keyboard.canRead--;
	return aux;
}

void process_scancode(int scancode){
	char aux;
	switch(scancode){
		case CAPSLOCK:{
			keyboard.caps=keyboard.caps*(-1)+1;
			break;
		}
		case LEFT_SHIFT_PRESSED:
		case RIGHT_SHIFT_PRESSED:{
			keyboard.shifted=1;
			break;
		}
		case LEFT_SHIFT_RELEASED:
		case RIGHT_SHIFT_RELEASED:{
			keyboard.shifted=0;
			break;
		}
		default:{
			
			if(is_makecode(scancode) && (keyTable[scancode][0] != NOT_PRINTABLE)){
				if(is_letter(scancode)){
					if((!keyboard.caps && keyboard.shifted) || (keyboard.caps && !keyboard.shifted)){
						aux = keyTable[scancode][1];
					}
					else{
						aux = keyTable[scancode][0];
					}
				}
				else{
					if(keyboard.shifted){
						aux = keyTable[scancode][1];
					}
					else{
						aux = keyTable[scancode][0];
					}
				}
				add_to_buffer(aux);
			}
			break;
		}
	}
}

int character_read(char* c){
	*c=read_from_buffer();
	return 1;
}

