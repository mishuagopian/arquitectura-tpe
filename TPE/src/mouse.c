#include "../include/mouse.h"
#include "../include/shell.h"
#include "../include/libc.h"
#include "../include/defs.h"

Mouse mouse;
int mouse_state=0;


void refresh_mouse(){
	static signed char delta_x,delta_y,flags;
	int x_final,y_final;
	switch(mouse_state){
		case 0:{
			flags=_inb(0x60) & 0x0FF;
			if((flags & 8)==0){
				mouse_state=0;
				return;
			}
			mouse_state++;			
			break;
		}
		case 1:{
			delta_x=_inb(0x60) & 0x0FF;
			mouse_state++;
			break;
		}
		case 2:{
			delta_y=_inb(0x60) & 0x0FF;
			mouse_state=0;
			if((flags & 1)!=0 ){
				mouse.l_button_pressed=1;
			}
			if((flags & 2)!=0){
				mouse.r_button_pressed=1;
			}
			if((flags & 4)!=0){
				mouse.m_button_pressed=1;
			}
			x_final=(delta_x%5)+mouse.x;
			x_final=x_final>79?79:x_final;
			x_final=x_final<0?0:x_final;
			y_final=(delta_y%5)*(-1)+mouse.y;
			y_final=y_final>24?24:y_final;
			y_final=y_final<0?0:y_final;
			mouse_cursor(mouse.x,mouse.y,x_final,y_final);
			mouse.x=x_final;
			mouse.y=y_final;
			break;
		}
		default:{
			break;
		}	
	}
}


unsigned char mouse_read(){			//sacado de osdev.org
  //Get's response from mouse
  mouse_wait(0);
  return _inb(0x60);
}

void mouse_wait(unsigned char a_type){	 	//sacado de osdev.org
  unsigned int _time_out=100000;
  if(a_type==0)
  {
    while(_time_out--) //Data
    {
      if((_inb(0x64) & 1)==1)
      {
        return;
      }
    }
    return;
  }
  else
  {
    while(_time_out--) //Signal
    {
      if((_inb(0x64) & 2)==0)
      {
        return;
      }
    }
    return;
  }
}

void mouse_write(unsigned char a_write){	//sacado de osdev.org
  //Wait to be able to send a command
  mouse_wait(1);
  //Tell the mouse we are sending a command
  _outb(0x64, 0xD4);
  //Wait for the final part
  mouse_wait(1);
  //Finally write
  _outb(0x60, a_write);
}

void initialize_mouse(){		//sacado de osdev.org
  unsigned char _status;
  //Enable the auxiliary mouse device
  mouse_wait(1);
  _outb(0x64, 0xA8);
  //Enable the interrupts
  mouse_wait(1);
  _outb(0x64, 0x20);
  mouse_wait(0);
  _status=(_inb(0x60) | 2);
  mouse_wait(1);
  _outb(0x64, 0x60);
  mouse_wait(1);
  _outb(0x60, _status);
  //Tell the mouse to use default settings
  mouse_write(0xF6);
  mouse_read();  //Acknowledge
  //Enable the mouse
  mouse_write(0xF4);
  mouse_read();  //Acknowledge

	mouse.x=40;
	mouse.y=12;
	mouse.l_button_pressed=0;
	mouse.r_button_pressed=0;
	mouse.m_button_pressed=0;	

}


unsigned char visual_mouse_read(char select){
	switch(select){
		case GET_X:{
			return mouse.x;
			break;
		}
		case GET_Y:{
			return mouse.y;
			break;
		}
		case GET_L_BOT:{
			if(mouse.l_button_pressed!=0){
				mouse.l_button_pressed=0;
				return 1;
			}
			return 0;
			break;
		}
		case GET_R_BOT:{
			if(mouse.r_button_pressed!=0){
				mouse.r_button_pressed=0;
				return 1;
			}
			return 0;
			break;
		}
		case GET_M_BOT:{
			if(mouse.m_button_pressed!=0){
				mouse.m_button_pressed=0;
				return 1;
			}
			return 0;
			break;
		}
		default:{
			return -1;
			break;
		}
	}
}

