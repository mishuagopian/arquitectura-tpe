#include "../include/visual.h"
#include "../include/defs.h"

Visual visual;
extern int mode;
char* prompt="MiShell: ~$  ";

void clear_screen() 
{
	unsigned int i=0;
	visual.cursor=0;
	while(i < (SCREEN_SIZE)){
		visual.vidmem[i]=' ';
		i++;
		visual.vidmem[i]=WHITE_TXT;
		i++;
	}
}

void hide_cursor(){
	_outb(0x3D4,0x0F);
	_outb(0x3D5,(unsigned char)(SCREEN_SIZE/2 & 0xFF));
	_outb(0x3D4,0x0E);
	_outb(0x3D5,(unsigned char)((SCREEN_SIZE/2 >> 8) & 0xFF));
}

void refresh_cursor(){
	_outb(0x3D4,0x0F);
	_outb(0x3D5,(unsigned char)(visual.cursor/2 & 0xFF));
	_outb(0x3D4,0x0E);
	_outb(0x3D5,(unsigned char)((visual.cursor/2 >> 8) & 0xFF));
}

void print_prompt(){
	printf(prompt);
	visual.cursor=visual.cursor-WIDTH*2+2*strlength(prompt);
	refresh_cursor();
}

void paint_exit(){
	int w = WIDTH*2*18+WIDTH-6,i,j;
	visual.mousescreen[w]='E';
	visual.mousescreen[w+4]='X';
	visual.mousescreen[w+8]='I';
	visual.mousescreen[w+12]='T';
	w++;
	for(i=-1;i<2;i++){
		for(j=-4;j<17;j+=2){
			visual.mousescreen[w+j+WIDTH*2*i] = WHITE_TXT | 0x60;
		}
	}
}

void paint_other_buttons(){
	int w =WIDTH*2*10+WIDTH,i,j;
	visual.mousescreen[w]='0';
	visual.mousescreen[w-14]='<';
	visual.mousescreen[w+14]='>';
	w++;
	for(i=-1;i<2;i++){
		for(j=-16;j<17;j+=2){
			if(j<12 && j>-12){
				visual.mousescreen[w+j+WIDTH*2*i] = WHITE_TXT | 0x50;
			}else{
				visual.mousescreen[w+j+WIDTH*2*i] = WHITE_TXT | 0x60;
			}		
		}
	}
}

void initialize_mouse_screen(){
	int i=0;
	while(i < (SCREEN_SIZE)){
		visual.mousescreen[i]=' ';
		i++;
		visual.mousescreen[i]=WHITE_TXT;
		i++;
	}
	paint_exit();
	paint_other_buttons();
}

void initialize_screensaver(){
	int i=0,w;	
	while(i < (SCREEN_SIZE)){
	visual.screensaver[i]=' ';
	i++;
	visual.screensaver[i]=WHITE_TXT;
	i++;
	}

	w=6*WIDTH*2+WIDTH-20;
	visual.screensaver[w]='P';
	visual.screensaver[w+2]='r';
	visual.screensaver[w+4]='o';
	visual.screensaver[w+6]='t';
	visual.screensaver[w+8]='e';
	visual.screensaver[w+10]='c';
	visual.screensaver[w+12]='t';
	visual.screensaver[w+14]='o';
	visual.screensaver[w+16]='r';
	visual.screensaver[w+18]=' ';
	visual.screensaver[w+20]='d';
	visual.screensaver[w+22]='e';
	visual.screensaver[w+24]=' ';
	visual.screensaver[w+26]='p';
	visual.screensaver[w+28]='a';
	visual.screensaver[w+30]='n';
	visual.screensaver[w+32]='t';
	visual.screensaver[w+34]='a';
	visual.screensaver[w+36]='l';
	visual.screensaver[w+38]='l';
	visual.screensaver[w+40]='a';

	w=9*WIDTH*2+WIDTH-8;
	visual.screensaver[w]='T';
	visual.screensaver[w+2]='P';
	visual.screensaver[w+4]='E';
	visual.screensaver[w+6]=' ';
	visual.screensaver[w+8]='A';
	visual.screensaver[w+10]='r';
	visual.screensaver[w+12]='q';
	visual.screensaver[w+14]='u';	
	visual.screensaver[w+16]='i';

	w=15*WIDTH*2+WIDTH-22;
	visual.screensaver[w]='A';
	visual.screensaver[w+2]='g';
	visual.screensaver[w+4]='o';
	visual.screensaver[w+6]='p';
	visual.screensaver[w+8]='i';
	visual.screensaver[w+10]='a';
	visual.screensaver[w+12]='n';
	visual.screensaver[w+14]=' ';
	visual.screensaver[w+16]='M';
	visual.screensaver[w+18]='i';
	visual.screensaver[w+20]='c';
	visual.screensaver[w+22]='h';
	visual.screensaver[w+24]='e';
	visual.screensaver[w+26]='l';
	visual.screensaver[w+28]=' ';
	visual.screensaver[w+30]='-';
	visual.screensaver[w+32]='-';
	visual.screensaver[w+34]=' ';
	visual.screensaver[w+36]='5';
	visual.screensaver[w+38]='4';
	visual.screensaver[w+40]='3';
	visual.screensaver[w+42]='2';
	visual.screensaver[w+44]='5';

	w=17*WIDTH*2+WIDTH-24;
	visual.screensaver[w]='D';
	visual.screensaver[w+2]='i';
	visual.screensaver[w+4]=' ';
	visual.screensaver[w+6]='N';
	visual.screensaver[w+8]='u';
	visual.screensaver[w+10]='c';
	visual.screensaver[w+12]='c';
	visual.screensaver[w+14]='i';
	visual.screensaver[w+16]=' ';
	visual.screensaver[w+18]='N';
	visual.screensaver[w+20]='i';
	visual.screensaver[w+22]='c';
	visual.screensaver[w+24]='o';
	visual.screensaver[w+26]='l';
	visual.screensaver[w+28]='a';
	visual.screensaver[w+30]='s';
	visual.screensaver[w+32]=' ';
	visual.screensaver[w+34]='-';
	visual.screensaver[w+36]='-';
	visual.screensaver[w+38]=' ';
	visual.screensaver[w+40]='5';
	visual.screensaver[w+42]='4';
	visual.screensaver[w+44]='0';
	visual.screensaver[w+46]='9';
	visual.screensaver[w+48]='1';

	w=19*WIDTH*2+WIDTH-20;
	visual.screensaver[w]='K';
	visual.screensaver[w+2]='l';
	visual.screensaver[w+4]='j';
	visual.screensaver[w+6]='e';
	visual.screensaver[w+8]='n';
	visual.screensaver[w+10]='a';
	visual.screensaver[w+12]='k';
	visual.screensaver[w+14]=' ';
	visual.screensaver[w+16]='I';
	visual.screensaver[w+18]='v';
	visual.screensaver[w+20]='a';
	visual.screensaver[w+22]='n';
	visual.screensaver[w+24]=' ';
	visual.screensaver[w+26]='-';
	visual.screensaver[w+28]='-';
	visual.screensaver[w+30]=' ';
	visual.screensaver[w+32]='5';
	visual.screensaver[w+34]='4';
	visual.screensaver[w+36]='0';
	visual.screensaver[w+38]='1';
	visual.screensaver[w+40]='9';
}

void initialize_screen(){
	unsigned int i=0;
	visual.active=1;
	visual.vidmem = (char *) SCREEN_START;
	clear_screen();
	initialize_screensaver();
	initialize_mouse_screen();
}

void delete_character(){
	visual.cursor-=2;
	visual.vidmem[visual.cursor]=' ';
	refresh_cursor();
}

void scroll_up(){
	int i,j;
	for(i=0,j=WIDTH*2;j<WIDTH*HEIGHT*2;i++,j++){
		visual.vidmem[i]=visual.vidmem[j];	
	}
	while(i<WIDTH*HEIGHT*2){
		visual.vidmem[i]=' ';
		visual.vidmem[i+1]=WHITE_TXT;
		i+=2;	
	}
	visual.cursor=(HEIGHT-1)*WIDTH*2;
	refresh_cursor();
}

void new_line(){
	visual.cursor=visual.cursor-(visual.cursor%(WIDTH*2))+WIDTH*2;
	if(visual.cursor>=SCREEN_SIZE){
		scroll_up();
	}
	refresh_cursor();
}

void show_character(char aux){
	visual.vidmem[visual.cursor]=aux;
	if(visual.cursor>=SCREEN_SIZE){
		scroll_up();
	}else{
		visual.cursor+=2;
	}
	refresh_cursor();
}

int write_string(char* buffer,int size){
	int i;
	for(i=0;i<size;i++){
		show_character(buffer[i]);
	}
	new_line();
	return i;
}
	
void goActive(){
	int i=0;
	while(i < (SCREEN_SIZE)){
		visual.vidmem[i]=visual.screen[i];	
		i++;		
	}
	if(mode==KEYBOARD_MODE){
		refresh_cursor();
	}	
	visual.active=1;
}

void goInactive(){
	int i=0;
	while(i < (SCREEN_SIZE)){
		visual.screen[i]=visual.vidmem[i];
		visual.vidmem[i]=visual.screensaver[i];		
		i++;		
	}	
	hide_cursor();
	visual.active=0;
}

void go_test_mouse(){
	int i=0;
	while(i < (SCREEN_SIZE)){
		visual.screen[i]=visual.vidmem[i];
		visual.vidmem[i]=visual.mousescreen[i];		
		i++;		
	}	
	hide_cursor();
}

void leave_test_mouse(){
	goActive();
	clear_screen();
}

void mouse_cursor(int prev_x,int prev_y,int new_x, int new_y){
	visual.vidmem[1+2*prev_x+2*WIDTH*prev_y]=visual.mousescreen[1+2*prev_x+2*WIDTH*prev_y];
	visual.vidmem[1+2*new_x+2*WIDTH*new_y]=CURSOR_FMT;
}

int visual_char_at(int x,int y){
	return visual.vidmem[2*x+2*WIDTH*y] & 0x0FF;
}

void write_at(int x, int y,int character){
	if(visual.active){	
		visual.vidmem[2*x+2*WIDTH*y]=(char)character;
	}	
	return;
}






